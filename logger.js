'use strict';
const winston = require('winston'),
  pack = require('./package'),
  moment = require('moment'),
  fs = require('fs'),
  log_dir = pack.logdir,
  separator = process.env.PUBLIC ? '\\' : '/';

let first = true;

const logger = winston.createLogger({
  level: 'info',
  transports: []
});

function getLogPath() {
  return (process.env.PUBLIC || process.env.HOME) +
    separator + log_dir + separator;
}

((logger, exports) => {

  const {combine, timestamp, printf, splat, simple} = winston.format;
  const formatter = printf(info => {
    return `${moment(info.timestamp).format("hh:mm:ssA")}:\t${info.message}`;
  });
  const fix = combine(splat(), timestamp(), formatter);

  function setupLoggerTransports(file) {
    logger
      .clear()
      .add(new winston.transports.File({
        filename: file,
        // handleExceptions: true,
        //humanReadableUnhandledException: true,
        format: fix
      }))
      .add(new winston.transports.Console({
        filename: file,
        format: combine(splat(), timestamp(), formatter)
      }))
    ;
  }

  function recycleLogger() {
    let d = new Date(new Date().toDateString());
    setupLogPathSync(d);
    setupLoggerTransports(getFileName());
    logger.info("\n\n**********************************************************************************");
    !first ? logger.log('error', 'Logger Renamed @ %s', new Date().toLocaleString()) : first = !first;
    //logger.info('Log Path: ' + getFileName(d));
    // logger.close();

    d.setHours(24);
    setTimeout(recycleLogger, d - Date.now() + 1000);
  }

  function getFileName(date) {
    return getLogPath()
      + moment(date).format('YYYY-MM MMMM-').replace(/-/g, separator)
      + (pack.name || '')
      + '-'
      + moment(date).format('YYYY-MM-DD')
      + '.log';
  }


  function setupLogPath(date, cbx) {

    var count = 0, arr = [
      getLogPath(),
      getLogPath() + date.getFullYear(),
      getLogPath() + date.getFullYear() + separator + moment(date).format('MM MMMM')];

    function done() {
      if (++count === arr.length)
        cbx();
    }

    arr.forEach(function (dir) {
      fs.mkdir(dir, done)
    });
  }

  function setupLogPathSync(date) {
    var arr = [
      getLogPath(),
      getLogPath() + date.getFullYear(),
      getLogPath() + date.getFullYear() + separator + moment(date).format('MM MMMM')];

    arr.forEach(function (dir) {
      fs.existsSync(dir) || fs.mkdirSync(dir);
    });
  }

  recycleLogger();
  exports.restart = recycleLogger;

})(logger, module.exports);

module.exports = logger;
const log = function (args) {
  logger.info(toString.call(args) === "[object String]" ? args : JSON.stringify(Array.prototype.slice.call(arguments)));
};
module.exports = {
  logResponse: (res, action, obj) => {
    let req = res.req,
      stringify = req.app.utility.methods.stringify,
      ref = req.headers['referer'] || req.headers['referrer'] || '',
      userAgent = req.headers['user-agent'] || '',
      info = `${req.userIP} ${req.method.toUpperCase()} ${req.originalUrl || req.url} HTTP/${req.httpVersionMajor + '.' + req.httpVersionMinor} ${ref} ${userAgent}`;
    if (req.isPost)
      info = `${info}, body:${stringify(req.body)}`;
    info = `${info}, Response: ${res.statusCode} ${stringify(obj)}`;
    log(info);
    res.json(obj);
  },
  logRequest: (req, action) => log((action || '') + ' Request: Params:\t%j\t, Query:\t%j\t, Body:\t%j.', req.params, req.query, req.body),
  log: log,
  error: log
};
console.log = log;
module.exports.logPath = getLogPath();
console.logResponse = module.exports.logResponse;
console.logRequest = module.exports.logRequest;
