var express = require('express');
var router = express.Router();
var  mongoose = require('mongoose');
var multer = require('multer');
const CheckAuth = require('../middleware/checkauth');
const eventcontroller = require('../controller/event');


const storage = multer.diskStorage({
    destination: function (req,file,cb) {
        cb(null, './uploadedevent/');
    },
    filename: function (req, file, cb){
        cb(null, new Date().toISOString() + file.originalname);
    }
});
const fileFilter = (req, res, cb) =>
{

if (file.mimeType === 'image/jpeg' || file.mimeType === 'image/png'){
    cb(null, true);
} else{
    cb(null, false);
}


};
const upload = multer({
    storage: storage,
    limits: {
    fileSize: 1024 * 1024 * 10,
        //fileFilter: fileFilter
    }

});

const Event = require('../models/Events');

router.get('/',CheckAuth,eventcontroller.event_get);


router.post('/',upload.single('Upload'),CheckAuth,eventcontroller.event_post);

router.get('/:eventid',CheckAuth, eventcontroller.event_get_id);

router.put('/:id/update',eventcontroller.event_put);

router.delete('/:eventid',CheckAuth, eventcontroller.event_delete);





module.exports = router;

