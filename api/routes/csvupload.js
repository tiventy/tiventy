var express = require('express');
var router = express.Router();
var  mongoose = require('mongoose');
var multer = require('multer');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const facebookStrategy = require('passport-facebook').Strategy;
const CheckAuth = require('../middleware/checkauth');
const usercontroller = require('../controller/uploadcsv');
//const CheckAuth = require('../middleware/checkauth');
//const fs = require('fs');

var storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
var upload = multer({storage: storage});
router.post('/do', upload.single('file'),CheckAuth,usercontroller.get_csv);


module.exports = router;