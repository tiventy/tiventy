var express = require('express');
 var router = express.Router();
 var  mongoose = require('mongoose');
 var bcrypt = require('bcrypt-nodejs');
 var multer = require('multer');
 const jwt = require('jsonwebtoken');
 const passport = require('passport');
 const facebookStrategy = require('passport-facebook').Strategy;
 const CheckAuth = require('../middleware/checkauth');

 const upload = multer({dest: 'uploads/'});
 const usercontroller = require('../controller/user');

 const Users = require('../models/users');


//const Login = require('../models/login');

// const storage = multer.diskStorage({
//     destination: function (req,file,cb) {
//         cb(null, './uploadedevent/');
//     },
//     filename: function (req, file, cb){
//         cb(null, new Date().toISOString() + file.originalname);
//     }
// });
// const fileFilter = (req, res, cb) =>
// {
//
//     if (file.mimeType === 'image/jpeg' || file.mimeType === 'image/png'){
//         cb(null, true);
//     } else{
//         cb(null, false);
//     }
//
//
// };
// const upload = multer({
//     storage: storage,
//     limits: {
//         fileSize: 1024 * 1024 * 10,
//         //fileFilter: fileFilter
//     }
//
// });



router.put('/:id/update',CheckAuth,usercontroller.user_put);

router.get('/',CheckAuth,usercontroller.user_get_all );

 router.post('/signup',upload.single('EventImage'),usercontroller.user_post);

 router.post('/login',usercontroller.user_login);

 router.get('/:userid',CheckAuth,usercontroller.user_get_id);
 router.delete('/:userid',CheckAuth,usercontroller.user_delete);

 router.post('/facebook/auth',passport.authenticate('facebook'),CheckAuth,usercontroller.facebook);




module.exports = router;