var express = require('express');
var router = express.Router();
var  mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');
const CheckAuth = require('../middleware/checkauth');
const OrgAuth = require('../middleware/OrganiserAuth');

const Organisers = require('../models/Organisers');

const organisercontroller = require('../controller/organiser');

router.get('/',CheckAuth,organisercontroller.organiser_get);


router.post('/signup',CheckAuth,organisercontroller.organiser_post);
router.post('/login',organisercontroller.organiser_login);


router.get('/:organiserid', CheckAuth,organisercontroller.organiser_get_id);

router.put('/:id/update',CheckAuth,organisercontroller.organiser_put);

router.delete('/:organiserid',CheckAuth,organisercontroller.organiser_delete);

module.exports = router;
