const mongoose = require('mongoose');


const eventSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
   // Pass:{type: mongoose.Schema.Types.ObjectId, ref: 'PassDetails'},
    EventType:{type: String, required: true},
    EventTitle:{type: String, required: true},
    // Past:{type: String, required: false},
    // Present:{type: String, required: false},
    EventDate:{type: String, required: true},
    EventTime:{type: String, required: true},
    EventVenue:{type: String, required: true},
    EventAddress:{type: String, required: true},
    CoverImage:{type: String, required: true},

    created: {
        type: Date,
        default: Date.now
    }

    //Date: Date()

/*
* []
* {
* "Passed":[],
* "Pending":[],
* "Ongoing":[]
* }
* */


});

module.exports = mongoose.model('Events', eventSchema);