const mongoose = require('mongoose');

const EventsAttendedSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    EventTitle:{type: String, required: true, unique: true,
    },
    TicketType:{type: String, required: true},
    Qrcode:{type: String, required: true},
    created: {
        type: Date,
        default: Date.now
    }

    //Date: Date()




});

module.exports = mongoose.model('EventsAttended', EventsAttendedSchema);