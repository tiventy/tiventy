const mongoose = require('mongoose');

const PassSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    //Header:{type: String, required: true},
    User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    Event:{type: mongoose.Schema.Types.ObjectId, ref: 'Events'},
    PhoneNumber:{type: String, required: true, unique: true},
    FullName:{type: String, required: true,
    },Email:{type: String, required: true,unique: true
    },
    TicketType: {type: String},
    Checkin:{type: String},
    Qrcode:{type: String},

    created: {
        type: Date,
        default: Date.now
    }

    //Date: Date()




});

module.exports = mongoose.model('PassDetails', PassSchema);