const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');


const organiserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    User:{type: mongoose.Schema.Types.ObjectId, ref: 'Users', required: true },
    email:{type: String, required: true, unique: true,
    },
    password:{type: String, required: true},
    created: {
        type: Date,
        default: Date.now
    }

    //Date: Date()




});
const hashPassword = (password, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        if (err) return callback(err);
        bcrypt.hash(password, salt, null, callback);
    });
};
organiserSchema.statics.hashPassword = hashPassword;

organiserSchema.pre('save', function (next) {
    let self = this;
    if (this.isModified("password"))
        hashPassword(this.password, (e, p) => {
            self.password = p;
            next();
        });
    else next();
});
let config = {
    transform: function (doc, ret) {
        delete ret.password;
        delete ret.token;
        delete ret["__v"];
        return ret;
    }
};
organiserSchema.set('toJSON', config);
organiserSchema.set('toObject', config);
module.exports = mongoose.model('Organisers', organiserSchema);