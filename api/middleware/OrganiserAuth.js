var jwt = require('jsonwebtoken');

module.exports=(req, res, next)=>{
    try {
        // const token = req.headers.authorization;
        // console.log(token);
        const token = req.headers.authorization.split(" ")[1];
        const decode = jwt.verify(token,process.env.JWT_TOKEN);
        req.Orgdata = decode;


        next();

    }catch (error) {
        return res.status(404).json({
            message: "authentication failed"
        });
    }






};