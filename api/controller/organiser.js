const Organisers = require('../models/Organisers');
var  mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
var bcrypt = require('bcrypt-nodejs');

exports.organiser_post = (req, res, next) => {
    var id = req.Userdata.userId;
    console.log(id);
    Organisers.find({email: req.body.email}).exec().then(user => {
        if (user.length >= 1) {
            return res.status(500).json({
                ResponseCode: "422",
                message: "organiser email already exist"
            })
        }
        else {
            const organiser = new Organisers({
                _id: new mongoose.Types.ObjectId(),
                User: req.Userdata.userId,
                email: req.body.email,
                password: req.body.password

            });

            organiser
                .save()
                .then(result => {
                    console.log(result);
                })
                .catch(err =>
                    console.log(err))
                    res.status(500).json({
                        ResponseCode: "500",
                        error: err
                    });

            res.status(200).json({
                ResponseCode: "200",
                message: "organizer created successfully",
                createdorganiser: organiser
            });


        }
    })
}



exports.organiser_get = (req, res, next) =>
{
    var id = req.Userdata.userId;

    console.log(id);

    Organisers.find({User:id}).exec().then(doc =>{
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            doc: doc})
    }).catch(err =>{
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}

exports.organiser_get_id = (req,res, next) =>{

    const id = req.params.organiserid
    var vid = req.Userdata.userId;


    Organisers.find({User:vid} || findById(id)).exec()
        .then(doc =>{
            res.status(200).json({
                ResponseCode: "200",
                message: doc
            })
        }).catch(error =>{
            res.status(500).json({
                ResponseCode: "500",
                error: error
            })

        }
    );
}

exports.organiser_put = (req,res,next)=>{

    var id = req.params.id;
    var vid = req.Userdata.userId;

    Organisers.findByIdAndUpdate(id || find({User:vid}), {$set: req.body}).select('email name').then(user=>{

        console.log(user)
        res.status(200).json({
            ResponseCode: "200",
            message: "organiser updated " + user
        })
    }).catch(error => res.status(500).json({
        ResponseCode: "500",
        error: error
    }))
}

exports.organiser_login = (req, res, next) => {
    var email = req.body.email;
    //var phone = req.body.phone;

    Organisers.find({email}).exec().then(user => {
        if (user.length < 1) {
            return res.status(404).json({
                ResponseCode: "422",
                message: "incorrect username or password"
            });
        }
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
            if (err) {
                return res.status(404).json({
                    ResponseCode: "422",
                    message: "incorrect username or password"

                });
            }

            if (result) {
                const token = jwt.sign({

                        email: user[0].email,
                        userId: user[0]._id
                    }, process.env.JWT_TOKEN,
                    {
                        expiresIn: "1h"
                    });


                return res.status(200).json({
                    ResponseCode: "200",
                    message: "successful",
                    token: token,
                    user: user


                });
            }
            return res.status(404).json({
                ResponseCode: "401",
                message: "incorrect username or password"
            });
        })
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            ResponseCode: "500",
            error: err
        });

    });
}

exports.organiser_delete = (req, res, next)=>{

    var vid = req.Userdata.userId;


    const id = ({_id: req.params.organiserid})


        Organisers.remove(id || find({User: vid})).exec().then(doc => {
            res.status(200).json({
                ResponseCode: "200",
                message: "organiser deleted"
            })
        }).catch(error => {
            error: error
        });




}