const Event = require('../models/EventsAttended');
const mongoose = require('mongoose');


exports.event_get = (req, res, next) =>
{
    var id = req.Userdata.userId;

    console.log(id);

    Event.find({User:id}).exec().then(doc =>{
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            doc: doc})
    }).catch(err =>{
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}