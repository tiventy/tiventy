const Event = require('../models/Events');
const mongoose = require('mongoose');
//const Users = require('../models/users');


exports.event_post = (req, res, next) =>


{
    //console.log(req.file.path.toString());
    var vid = req.Userdata;
    console.log(vid);
    const event = new Event({
        _id: new mongoose.Types.ObjectId(),
        User: req.Userdata.userId,
        EventType: req.body.EventType,
        EventTitle: req.body.EventTitle,
        //CoverImage: req.file.path,
        CoverImage: req.body.CoverImage,
        EventDate: req.body.EventDate,
        EventTime: req.body.EventTime,
        EventVenue: req.body.EventVenue,
        EventAddress: req.body.EventAddress



    });

    event
        .save()
        .then(result => {
            //console.log(result);
            console.log("DONE");
        })
        .catch(err =>
            console.log(err));

    res.status(200).json({
        ResponseCode: "200",
        message: "event created successfully",
        createdevent : event
    });



}

exports.event_get = (req, res, next) =>
{
    var id = req.Userdata.userId;

    console.log(id);

    Event.find({User:id}).exec().then(doc =>{
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            doc: doc})
    }).catch(err =>{
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });
}

exports.event_get_id = (req,res, next) =>
{
    var vid = req.Userdata.userId;

    var id = req.params.eventid;

    Event.findById(id || find({User: vid})).exec().then(doc =>
    {
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            doc: doc
        })
    }).catch(err =>
    {
        console.log(err)
        res.status(500).json({ResponseCode: "200",error: err});
    });

}

exports.event_put = (req,res,next)=>{

    var id = req.params.id;
    var vid = req.Userdata.userId;


    Event.findByIdAndUpdate(id || find({User: vid}), {$set: req.body}).select('email name').then(user=>{

        console.log(user)
        res.status(200).json({
            ResponseCode: "200",
            message: "event updated" ,
            user:  user
        })
    }).catch(error => res.status(500).json({
        ResponseCode: "500",
        error: error}));
}

exports.event_delete = (req, res, next) =>
{
    var id = req.params.eventid;
    var vid = req.Userdata.userId;

    Event.remove(id || find({User:vid})).exec().then(doc =>{
        console.log(doc)
        res.status(200).json({
            ResponseCode: "200",
            message: "event deleted" ,
            doc: doc
        })
    }).catch(err =>{
        console.log(err)
        res.status(500).json({
            ResponseCode: "500",
            message: err
        })
    });

}