
const logger = require('./logger');
const express = require('express');
var acl = require('acl');
var Users = require('./api/models/users');

const app = express();
app.logger = logger;

const morgan = require('morgan');
const bodyparser = require('body-parser');
const  mongoose = require('mongoose');
const passport = require('passport');
const facebook = require('passport-facebook').Strategy;



//var mongoose = require('mongoose');
var url = 'mongodb://localhost:27017/Tiventy-api';

mongoose.connect(url);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.on('open', function () {
  console.log('connected');

});


acl = new acl(new acl.mongodbBackend(db, 'prefix'));



// acl.allow([
//     {
//         roles: ['user'],
//         allows: [
//             {
//                 resources: ['/api/events', '/api/categories'],
//                 permissions: ['get', 'post', 'put', 'delete'],
//             },
//         ],
//     },
//     {
//         roles: ['admin'],
//         allows: [
//             {
//                 resources: ['/api/users'],
//                 permissions: ['get', 'post', 'put', 'delete'],
//             },
//         ],
//     },
// ]);
acl.allow('admin', '/api/routes/user', ['GET', 'POST', 'PUT', 'DELETE'], error => {
    if (error) {
        console.log('Error while assigning permissions');
    }
    console.log('Successfully assigned permissions to admin role');
});

Users
    .find()
    .exec()
    .then(users => {
        users.forEach(user => {
            acl.addUserRoles(user._id.toString(), user.role, err => {
                if (err) {
                    console.log(err);
                }
                console.log('Added', user.role, 'role to user', user.firstName, 'with id', user._id);
            });
        });
    });
Users
    .find()
    .exec()
    .then(users => {
        users.forEach(user => {
            acl.allowedPermissions(user._id.toString(), ['/api/routes/user'], (_, permissions) => {
                console.log(user.firstName, ' ', user.role);
                console.log(permissions);
            });
        });
    });
//mongoose.connect('mongodb+srv://sammy:' + process.env.MONGO_ATLAS_PW + '@tiventy-api-mc4p2.mongodb.net/test?retryWrites=true',{ useNewUrlParser: true } );



var userRoutes = require('./api/routes/user');
var eventRoutes = require('./api/routes/event');
var organisersRoutes = require('./api/routes/organiser');
const  uploadcsvRoutes = require('./api/routes/csvupload');
//var router = require('./api/routes/try');


app.use(morgan('dev'));
app.use(passport.initialize());
//app.use(passport.session());

app.use('/logs',express.static(app.logger.logPath),require('serve-index')(app.logger.logPath, { icons: true }));

app.use('/uploadedevent',express.static('uploadedevent'));
app.use(bodyparser.urlencoded({extended: false}));
app.use(bodyparser.json());

// app.use((req, res, next) => {
//     res.header('Access-Control-Allow-Origin', '*');
//     //res.header('Access-Control-Allow-Headers', "Origin, X-Requested-Width, Content-Type,Authorization");
//
// if (req.method === 'OPTIONS') {
//     res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, PATCH');
//     return res.status(200).json({});
// }
// });



app.use('/user', userRoutes);
app.use('/event', eventRoutes);
app.use('/organiser', organisersRoutes);
app.use('/csvupload', uploadcsvRoutes);


app.all('*',(req, res) =>
{
  console.log(`Not found ${req.url}`);
  res.status(404).end('Not found');
});

app.use((error, req, res) =>
{
  console.log(error);
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;